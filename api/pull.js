const { promisify: pify } = require(`util`)
const authCheck = require(`./_lib/auth`)
const DarkSky = require(`forecastio`)
const auth = require(`@qnzl/auth`)

const handler = async (req, res) => {
  const darksky = new DarkSky(process.env.DARKSKY_KEY)

  const { lat, long } = req.body

  if (!lat || !long) {
    return res.status(400).end()
  }

  const weather = await darksky.forecast(lat, long)

  return res.json(weather)
}

module.exports = (req, res) => {
  return authCheck(CLAIMS.weather.dump)(req, res)
}
